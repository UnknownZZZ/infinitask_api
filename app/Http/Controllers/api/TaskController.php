<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaskRequest;
use App\Models\Task;
use Illuminate\Http\Request;

class TaskController extends Controller
{

    /**
     * @param TaskRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTask(TaskRequest $request){
        //TODO: валідація, переписати на реквести валідейтед
        $params =[
            'task_date'=>$request->date,
            'task_title'=>$request->title,
            'task_description'=>$request->description,
            'task_status'=>'new',
        ];
        Task::create($params);

        //TODO: винести в трейт або в статичний метод
        return response()->json([
            'success' => true,
            'response' => 'Task has been created',
        ],201);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeTask(Request $request){
        //TODO: model route binding
        Task::where([['id', '=', $request->id]])->delete();
        return response()->json([
            'success' => true,
            'response' => 'Task has been removed',
        ],204);
    }

    public function updateTask(Request $request){
        $task = Task::where([['id', '=', $request->id]])->first();
        //TODO: request validated
        $task->task_title = $request->title;
        $task->task_description = $request->description;
        $task->save();
        return response()->json([
            'success' => true,
            'response' => 'Task has been updated',
        ],200);
    }


    public function markTaskAsDone(Request $request){
        $task = Task::where([['id', '=', $request->id]])->first();
        //TODO: request validated
        $task->task_status = 'done';
        $task->save();
        return response()->json([
            'success' => true,
            'response' => 'Task has been marked as done',
        ],201);
    }
}
