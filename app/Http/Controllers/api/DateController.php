<?php

namespace App\Http\Controllers\api;

use App\Domain\ValueObjects\Day;
use App\Http\Controllers\Controller;
use App\Models\Task;
use Illuminate\Http\Request;

class DateController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTasksPaginated(Request $request){
        //TODO: скоротити, додати валідацію
        $daysArray = [];
        $defaultPage = 1;
        $page = $defaultPage;
        $daysByPage = 5;
        if($request->has('page')) {
            $page = (int)$request->page;
        }

        $startPosition = $daysByPage*($page-1)+1;
        for($i=$startPosition;$i<=$daysByPage*$page;$i++){
            $day = new Day();
            $taksArray = [];
            //TODO: next day, масив днів через ренж
            $parsedDate = date("Y/m/d", strtotime('today -'.($i-1).' day'));
            $day->currentDate = $parsedDate;

            $allTaskByDay = Task::where([['task_date', '=', $parsedDate]])->get();
            if(count($allTaskByDay)>0){
                foreach ($allTaskByDay as $taskItem){
                    $task = new \App\Domain\ValueObjects\Task();
                    $task->taskId = $taskItem['id'];
                    $task->currentDate = $taskItem['task_date'];
                    $task->taskTitle = $taskItem['task_title'];
                    $task->taskDescription = $taskItem['task_description'];
                    $task->taskStatus = $taskItem['task_status'];
                    array_push($taksArray,$task);
                }
            }

            //$day->taskArray = $taksArray;
            $day->taskCount = count($taksArray);
            //TODO: ресурси
            array_push($daysArray,$day);
        }


        return response()->json([
            'success' => true,
            'response' => $daysArray,
        ],200);
    }


    public function getTasksByDate(Request $request){
        //TODO: скоротити, додати валідацію

        $allTaskByDay = Task::where([['task_date', '=', $request->date]])->get();


        return response()->json([
            'success' => true,
            'response' => $allTaskByDay,
        ],200);
    }

}
