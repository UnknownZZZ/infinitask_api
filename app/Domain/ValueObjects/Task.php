<?php
namespace App\Domain\ValueObjects;

use Illuminate\Support\Facades\Date;

class Task {
    public int $taskId;
    public string $currentDate;
    public string $taskTitle;
    public string $taskDescription;
    public string $taskStatus;
}
