<?php

use App\Http\Controllers\api\DateController;
use App\Http\Controllers\api\TaskController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/task/getAllPaginated', [DateController::class,'getAllTasksPaginated']);
Route::get('/task/getAllByDate', [DateController::class,'getTasksByDate']);
Route::post('/task', [TaskController::class,'createTask']);
Route::delete('/task', [TaskController::class,'removeTask']);
Route::put('/task', [TaskController::class,'updateTask']);
Route::post('/task/markasdone', [TaskController::class,'markTaskAsDone']);
